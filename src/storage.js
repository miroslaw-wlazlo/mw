class Connection {
  
  constructor(user) {
    this.user = user
  }
  
  repositoryAt(url) {
    return new Repository(this, url)
  }
}

class Repository {

  constructor(connection, url) {
    this.connection = connection
    this.url = url
  }

  getBranch(name) {
    return new Branch(repository, name)
  }
}

class Branch {

  constructor(repository, name) {
    this.repository = repository
    this.name = name
  }

  fileAt(path) {
    return new RepoFile(this, path)
  }
}

class RepoFile {

  constructor(branch, path) {
    this.branch = branch
    this.path = path
  }

  get url() {
    return this.branch.repository.url + '/files/' + encodeURI(this.path).replace(/\//g, '%2F').replace(/\./g, '%2E')
  }

  get config() {
    return {
      mode: 'cors',
      headers: {
        'Private-Token': this.branch.repository.connection.user.token,
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    }
  }

  async create(content, encoding) {
    const url = this.url + '?branch=' + this.branch.name + '&author_email=author%40example.com&author_name=Firstname%20Lastname&commit_message=create%20a%20new%20file'
    var response = (await fetch(url, { method: 'POST', body: JSON.stringify({
      content,
      encoding,
      author_email: this.branch.repository.connection.user.email,
      author_name: this.branch.repository.connection.user.name,
      commit_message: 'created file ' + this.path
    }), ...this.config })).json()
    console.log(response)
    return response
  }

  async read() {
    const url = this.url + '?ref=' + this.branch.name
    return (await fetch(url, { method: 'GET', ...this.config })).json()
  }

  async update(content, encoding) {
    const url = this.url + '?branch=' + this.branch.name + '&author_email=author%40example.com&author_name=Firstname%20Lastname&commit_message=create%20a%20new%20file'
    return (await fetch(url, { method: 'PUT', body: JSON.stringify({
      content,
      encoding,
      author_email: this.branch.repository.connection.user.email,
      author_name: this.branch.repository.connection.user.name,
      commit_message: 'updated file ' + this.path
    }), ...this.config })).json()
  }

  async delete() {
    const url = this.url + '?branch=' + this.branch.name + '&author_email=author%40example.com&author_name=Firstname%20Lastname&commit_message=create%20a%20new%20file'
    return (await fetch(url, { method: 'DELETE', ...this.config })).json()
  }
}

const connection = new Connection({
  token: 'b_H3rVX1TnmgDtdc6jFk',
  email: 'author@example.com',
  name: 'Firstname Lastname'
})

const repository = connection.repositoryAt('https://gitlab.com/api/v4/projects/4762607/repository')
const master = repository.getBranch('master')

export default {
  async storeBase64(path, content) {
    const file = master.fileAt(path)
    return file.create(content, 'base64')
  }
}
